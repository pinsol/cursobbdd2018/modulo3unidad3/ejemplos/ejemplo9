﻿USE ejmplo9;

-- Procedimiento que crea la tabla Empleados
DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablaEmpleados(test int)
BEGIN
  /* variables */
  /* cursores */
  /* excepciones */
  /* programa */
  CREATE OR REPLACE TABLE empleados(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  fechaNacimiento date,
  correo varchar(20),
  edad int,
  PRIMARY KEY (id)
  )ENGINE MYISAM;

  IF (test=1) THEN
    INSERT INTO empleados (nombre, fechaNacimiento)
  VALUES
      ('Pedro','2000-01-1'),
      ('Ana','1980-10-1'),
      ('Jose','1990-12-4'),
      ('Luis','1985-4-1'),
      ('Ernesto','1975-4-4');
  END IF;
END //
DELIMITER ;


-- Procedimiento que crea la tabla Empleados
DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablaEmpresa(test int)
BEGIN
  /* variables */
  /* cursores */
  /* excepciones */
  /* programa */
  CREATE OR REPLACE TABLE empresa(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  direccion varchar(20),
  numeroEmpleados int,
  PRIMARY KEY (id)
  )ENGINE MYISAM;

  IF (test=1) THEN
    INSERT INTO empresa (nombre, direccion)
  VALUES
      ('empresa1','direccion1'),
      ('empresa2','direccion2'),
      ('empresa3','direccion3'),
      ('empresa4','direccion4'),
      ('empresa5','direccion5');
  END IF;
END //
DELIMITER ;


-- Procedimiento que crea la tabla Empleados
DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablaTrabajan(test int)
BEGIN
  /* variables */
  /* cursores */
  /* excepciones */
  /* programa */
  CREATE OR REPLACE TABLE trabajan(
  id int AUTO_INCREMENT,
    empleado int,
    empresa int,
  fechaInicio date,
  fechaFin date,
  baja bool DEFAULT FALSE,
  PRIMARY KEY (id),
  CONSTRAINT trabajanUK UNIQUE KEY (empleado,empresa)
  )ENGINE MYISAM;

  IF (test=1) THEN
      INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin)
  VALUES (1, 0, CURDATE(), CURDATE(), FALSE);
END //
DELIMITER ;
