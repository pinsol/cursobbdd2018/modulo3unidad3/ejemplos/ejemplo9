﻿/**
  Ejemplo 9
**/

/* Crear todas las tablas utilizando el motor de almacenamiento MYISAM y codificacion
   de caracteres en utf8 y castellano. (Todas las tablas */

-- 1a.- 
CREATE DATABASE IF NOT EXISTS ejemplo9;
USE ejemplo9;

-- 1b.-
CALL crearTablaEmpleados(0);  -- tabla vacia
CALL crearTablaEmpleados(1);
SELECT * FROM empleados;

-- 1c.-
CALL crearTablaEmpresa(0);  -- tabla vacia
CALL crearTablaEmpresa(1);
SELECT * FROM empresa;

-- 1.d.-
CALL crearTablaTrabajan(0);
CALL crearTablaTrabajan(1);
SELECT * FROM trabajan;
